package me.zyrkran.customwarp.utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Chat {
	
	public static void sendMessage(Player player, String msg){
		player.sendMessage(color(msg));
	}
	
	public static String color(String string){
		string = ChatColor.translateAlternateColorCodes('&', string);
		return string;
	}

}
