package me.zyrkran.customwarp.exceptions;

public class WarpAlreadyExistsException extends Exception{

	private static final long serialVersionUID = 8426700580503598346L;
	private String message; 
	
	public WarpAlreadyExistsException(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
}
