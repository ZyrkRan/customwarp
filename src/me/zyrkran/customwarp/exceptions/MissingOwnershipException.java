package me.zyrkran.customwarp.exceptions;

public class MissingOwnershipException extends Exception{

	private static final long serialVersionUID = 3708335950482579512L;
	private String message; 
	
	public MissingOwnershipException(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}
