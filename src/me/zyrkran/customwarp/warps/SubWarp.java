package me.zyrkran.customwarp.warps;

import java.util.UUID;

import org.bukkit.Location;

public class Subwarp {

	private Warp parent;
	
	private UUID creator;
	private String name;
	private Location location;

	public Subwarp(UUID creator, String name, Location loc){
		this.creator = creator;
		this.name = name;
		this.location = loc;
	}

	public Warp getParent() {
		return parent;
	}

	public void setParent(Warp parent) {
		this.parent = parent;
	}

	public UUID getCreatorUUID() {
		return creator;
	}

	public void setOwner(UUID owner) {
		this.creator = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getWorldName() {
		return location.getWorld().getName();
	}

	public double getX() {
		return location.getX();
	}

	public double getY() {
		return location.getY();
	}

	public double getZ() {
		return location.getZ();
	}

	public double getYaw() {
		return location.getYaw();
	}

	public double getPitch() {
		return location.getPitch();
	}

}

