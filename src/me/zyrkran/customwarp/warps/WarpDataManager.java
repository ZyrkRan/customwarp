package me.zyrkran.customwarp.warps;

import java.util.List;

import me.zyrkran.customwarp.config.Config;

class WarpDataManager {
	
	public static void saveWarpToConfig(Config config, Warp warp){
		config.getConfig().set("warps." + warp.getName() + ".creator", warp.getCreatorUUID().toString());
		config.getConfig().set("warps." + warp.getName() + ".world", warp.getWorldName());
		config.getConfig().set("warps." + warp.getName() + ".x", warp.getX());
		config.getConfig().set("warps." + warp.getName() + ".y", warp.getY());
		config.getConfig().set("warps." + warp.getName() + ".z", warp.getZ());
		config.getConfig().set("warps." + warp.getName() + ".yaw", warp.getYaw());
		config.getConfig().set("warps." + warp.getName() + ".pitch", warp.getPitch());
		config.saveConfig(); // save config file
	}
	
	public static void removeWarpFromConfig(Config config, Warp warp){
		config.getConfig().set("warps." + warp.getName(), null);
		config.saveConfig(); // save config file
	}
	
	public static Warp getWarpByName(String name, List<Warp> warpList){
		for (Warp warp : warpList){
			if (warp.getName().equalsIgnoreCase(name)){
				return warp;
			}
		}
		return null; // didn't find warp
	}
}
