package me.zyrkran.customwarp.warps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.zyrkran.customwarp.CustomWarp;
import me.zyrkran.customwarp.commands.CommandManager;
import me.zyrkran.customwarp.config.Config;
import me.zyrkran.customwarp.exceptions.InvalidStringException;
import me.zyrkran.customwarp.exceptions.MissingOwnershipException;
import me.zyrkran.customwarp.exceptions.SubWarpNotFoundException;
import me.zyrkran.customwarp.exceptions.WarpAlreadyExistsException;
import me.zyrkran.customwarp.exceptions.WarpNotFoundException;
import me.zyrkran.customwarp.messages.Messages;
import me.zyrkran.customwarp.utils.Chat;
import net.md_5.bungee.api.ChatColor;

public class WarpManager {

	private static List<Warp> warps;
	private Config warpConfig;
	
	public WarpManager(CustomWarp plugin){
		warps = new ArrayList<Warp>();
		warpConfig = new Config(plugin, "warps.yml");
		warpConfig.saveDefaultConfig();
	}
	
	public List<Warp> getWarps() {
		return warps;
	}

	public Config getWarpConfig(){
		return warpConfig;
	}
	
	/**
	 *  Process player request and register a new warp if everything is fine
	 */
	public void tryCreateWarp(Player player, String name, Location loc) throws InvalidStringException, WarpAlreadyExistsException{
		if (!StringUtils.isAlphanumeric(name)){
			throw new InvalidStringException("Warp name must be alphanumeric");
		}
		
		// Check if warp exists
		Warp warp = new Warp(player.getUniqueId(), name, loc);
		for (Warp w : warps){
			if (w.getName().equalsIgnoreCase(warp.getName())){
				throw new WarpAlreadyExistsException("A warp with this name already exists!");
			}
		}

		// Everything is fine, create Warp objects
		createWarp(warp);
	}

	/** 
	 * Save warp data to config and add it to memory
	 * @param warp
	 */
	private void createWarp(Warp warp){
		CommandManager.registerCommand(warp.getName().toLowerCase());
		WarpDataManager.saveWarpToConfig(warpConfig, warp);
		getWarps().add(warp);
	}
	
	/**
	 *  Process player request and remove warp if exists
	 */
	public void tryRemoveWarp(Player player, String name) throws InvalidStringException, WarpNotFoundException{
		if (!StringUtils.isAlphanumeric(name)){
			throw new InvalidStringException("Warp names are alphanumeric!");
		}
		
		// Check if warps exists
		for (Warp warp : getWarps()){
			if (warp.getName().equalsIgnoreCase(name)){
				removeWarp(warp);
				return;
			}
		}

		throw new WarpNotFoundException();
	}
	
	/**
	 * Erase warp data from config and memory
	 * @param name
	 */
	private void removeWarp(Warp warp){
		CommandManager.unregisterCommand(warp.getName().toLowerCase());
		WarpDataManager.removeWarpFromConfig(warpConfig, warp);
		getWarps().remove(warp);
	}
	
	/**
	 *  Process player request and create a new subwarp within a warp
	 */
	public void tryCreateSubWarp(Player player, String subwarp_name, String parent) throws InvalidStringException, MissingOwnershipException{
		if (!StringUtils.isAlphanumeric(subwarp_name)){
			throw new InvalidStringException("Subwarp names must be alphanumeric!");
		}
		
		Warp wparent = getWarpByName(parent);
		Subwarp sw = new Subwarp(player.getUniqueId(), subwarp_name, player.getLocation());
		
		for (Subwarp s : wparent.getSubwarps()){
			if (s.getName().equalsIgnoreCase(subwarp_name)){
				try {
					updateSubwarpLocation(player, warpConfig, wparent, subwarp_name, player.getLocation());
				} catch (SubWarpNotFoundException e) {
					e.printStackTrace();
				} catch (MissingOwnershipException e){
					throw new MissingOwnershipException("Player is not the owner of the subwarp!");
				}
				return;
			}
		}
		
		for (Warp w : warps){
			// Check for parent warp
			if (w.getName().equalsIgnoreCase(wparent.getName())){
				w.createSubWarp(warpConfig, sw);
				return;
			}
		}
	}
	
	/**
	 *  Process player request and remove subwarp if it exists
	 */
	public void tryRemoveSubWarp(Player player, String name, String parent) throws SubWarpNotFoundException{
		if (!StringUtils.isAlphanumeric(name)){
			Chat.sendMessage(player, Messages.SUBWARP_NAME_MUST_BE_ALPHANUMERIC);
			return;
		}
		
		Warp wparent = getWarpByName(parent);
		
		for (Subwarp sw : wparent.getSubwarps()){
			if (sw.getName().equalsIgnoreCase(name)){
				wparent.removeSubwarp(warpConfig, sw);
				return;
			}
		}
		
		throw new SubWarpNotFoundException();
	}
	
	
	/**
	 * This method is used to update warp spawning location and saving changes to config file
	 * @param name - warp name 
	 * @param location - new location
	 * @throws WarpNotFoundException 
	 */
	public void updateWarpLocation(String name, Location location) throws WarpNotFoundException{
		Warp warp = getWarpByName(name);
		
		// update memory data
		warp.setLocation(location);
		
		// Save changes to config file
		WarpDataManager.saveWarpToConfig(warpConfig, warp);
	}
	
	/**
	 * This method is used to update subwarp spawning location and saving changes to config file
	 * @param parent - parent warp
	 * @param location - new location
	 * @throws WarpNotFoundException 
	 */
	public void updateSubwarpLocation(Player player, Config config, Warp parent, String name, Location location) throws SubWarpNotFoundException, MissingOwnershipException{		
		// check if player is the owner
		if (!player.getUniqueId().toString().equalsIgnoreCase(parent.getCreatorUUID().toString())){
			throw new MissingOwnershipException("Player is not the owner of the warp!");
		}
		
		parent.updateSubwarpLocation(config, name, location);
	}
	
	/**
	 * This method is called from the onEnable() in main class
	 * to load warp/subwarp data from the config when the plugin enables
	 */
	public void loadWarpDataFromConfig() {
		if (warpConfig.getConfig().getConfigurationSection("warps") == null){
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[CustomWarp] No warp information found, assuming file is empty!");
			return;
		}
		
		// Grab all warps available in config file
		Set<String> savedWarps = warpConfig.getConfig().getConfigurationSection("warps").getKeys(false);
		Location loc = null;
		Warp warp = null;
		
		for (String s : savedWarps){
			UUID cuuid = UUID.fromString(warpConfig.getConfig().getString("warps." + s + ".creator"));
			loc = new Location(
					Bukkit.getWorld(warpConfig.getConfig().getString("warps." + s + ".world")),
					warpConfig.getConfig().getDouble("warps." + s + ".x"),
					warpConfig.getConfig().getDouble("warps." + s + ".y"),
					warpConfig.getConfig().getDouble("warps." + s + ".z")
					);
			loc.setYaw((float)warpConfig.getConfig().getDouble("warps." + s + ".yaw"));
			loc.setPitch((float)warpConfig.getConfig().getDouble("warps." + s + ".pitch"));
			
			// Create warp object 
			warp = new Warp(cuuid, s, loc);
			
			// Try to load subwarps if any
			if (warpConfig.getConfig().getConfigurationSection("warps." + s + ".subwarps") == null){
				getWarps().add(warp); // Just add the warp to memory
				continue;
			}
			
			// Grab all subwarps available in config file
			Set<String> savedSubwarps = warpConfig.getConfig().getConfigurationSection("warps." + s + ".subwarps").getKeys(false);
			Location sub_loc = null;
			Subwarp swarp = null;
			
			for (String st : savedSubwarps){
				sub_loc = new Location(
						Bukkit.getWorld(warpConfig.getConfig().getString("warps." + s + ".subwarps." + st+ ".world")),
						warpConfig.getConfig().getDouble("warps." + s + ".subwarps." + st + ".x"),
						warpConfig.getConfig().getDouble("warps." + s + ".subwarps." + st + ".y"),
						warpConfig.getConfig().getDouble("warps." + s + ".subwarps." + st + ".z")
						);
				sub_loc.setYaw((float)warpConfig.getConfig().getDouble("warps." + s + ".subwarps." + st + ".yaw"));
				sub_loc.setPitch((float)warpConfig.getConfig().getDouble("warps." + s + ".subwarps." + st + ".pitch"));
				
				// Create SubWarp Object
				swarp = new Subwarp(
						UUID.fromString(warpConfig.getConfig().getString("warps." + s + ".subwarps." + st + ".creator")),
						st, sub_loc);
				
				// Add subwarp to the warp object
				warp.addSubwarp(swarp);
			}
			
			// Finally, add the warp Object to the memory
			getWarps().add(warp);
		}
	}
	
	
	/** 
	 * Get instance of an existing warp by name
	 * @param name
	 * @return
	 */
	public static Warp getWarpByName(String name){
		for (Warp w : warps){
			if (w.getName().equalsIgnoreCase(name)){
				return w;
			}
		}
		return null;
	}
	
	
	/**
	 * Get a case-sensitive String of the warp's name
	 * @param string
	 * @return
	 */
	public static String getWarpCaseSentitiveName(String string){
		for (Warp w : warps){
			if (w.getName().equalsIgnoreCase(string)){
				return w.getName();
			}
		}
		return null;
	}
	
	public static List<Subwarp> getTotalWarpsByPlayer(Player player){
		List<Subwarp> warpsByPlayer = new ArrayList<Subwarp>();
		
		for (Warp warp : warps){
			for (Subwarp subwarp : warp.getSubwarps()){
				if (subwarp.getCreatorUUID().toString().equalsIgnoreCase(player.getUniqueId().toString())){
					warpsByPlayer.add(subwarp);
				}
			}
		}
		return warpsByPlayer;
	}
	
	public static boolean playerHasPermissionToSetWarp(Player player){
		int currentWarps = getTotalWarpsByPlayer(player).size();
		int maxWarps = 0;
		for (int i=0; i > 100; i++){
			if (player.hasPermission("customwarp.setsubwarp." + i)){
				maxWarps = i;
				break;
			}
		}
		
		if (currentWarps >= maxWarps){
			return false;
		}
		
		return true;
	}
}