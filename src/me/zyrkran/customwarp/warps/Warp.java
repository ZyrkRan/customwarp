package me.zyrkran.customwarp.warps;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;

import me.zyrkran.customwarp.config.Config;

public class Warp {

	private UUID creator;
	private String name;
	private Location location;

	private List<Subwarp> subwarps;

	public Warp(UUID uuid, String name, Location loc) {
		this.creator = uuid;
		this.name = name;
		this.location = loc;
		this.subwarps = new ArrayList<Subwarp>();
	}

	public String getName() {
		return name;
	}

	public UUID getCreatorUUID() {
		return creator;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location loc) {
		this.location = loc;
	}

	public String getWorldName() {
		return location.getWorld().getName();
	}

	public double getX() {
		return location.getX();
	}

	public double getY() {
		return location.getY();
	}

	public double getZ() {
		return location.getZ();
	}

	public double getYaw() {
		return location.getYaw();
	}

	public double getPitch() {
		return location.getPitch();
	}

	public List<Subwarp> getSubwarps() {
		return subwarps;
	}
	
	public Subwarp getSubwarpByName(String name){
		for (Subwarp s : subwarps){
			if (s.getName().equalsIgnoreCase(name)){
				return s;
			}
		}
		return null;
	}

	public void createSubWarp(Config config, Subwarp subwarp){	
		// set parent
		subwarp.setParent(this);

		// save data to config
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".creator",
				subwarp.getCreatorUUID().toString());
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".world",
				subwarp.getWorldName());
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".x",
				subwarp.getX());
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".y",
				subwarp.getY());
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".z",
				subwarp.getZ());
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".yaw",
				subwarp.getYaw());
		config.getConfig().set("warps." + subwarp.getParent().getName() + ".subwarps." + subwarp.getName() + ".pitch",
				subwarp.getPitch());
		config.saveConfig(); // save config file

		// add subwarp to memory
		subwarps.add(subwarp);
	}
	
	public void updateSubwarpLocation(Config config, String name, Location location){
		Subwarp subwarp = getSubwarpByName(name);
		subwarp.setLocation(location);
		
		// save data to config
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".creator", subwarp.getCreatorUUID().toString());
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".world", subwarp.getWorldName());
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".x", subwarp.getX());
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".y", subwarp.getY());
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".z", subwarp.getZ());
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".yaw", subwarp.getYaw());
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName() + ".pitch", subwarp.getPitch());
		config.saveConfig(); // save config file
		
	}

	public void removeSubwarp(Config config, Subwarp subwarp) {
		config.getConfig().set("warps." + getName() + ".subwarps." + subwarp.getName(), null);
		config.saveConfig();

		// remove from memory
		subwarps.remove(subwarp);
	}

	public void addSubwarp(Subwarp subwarp) {
		this.subwarps.add(subwarp);
	}

	public void saveWarpToConfig(Config config, Warp warp) {
		config.getConfig().set("warps." + warp.getName() + ".creator", warp.getCreatorUUID().toString());
		config.getConfig().set("warps." + warp.getName() + ".world", warp.getWorldName());
		config.getConfig().set("warps." + warp.getName() + ".x", warp.getX());
		config.getConfig().set("warps." + warp.getName() + ".y", warp.getY());
		config.getConfig().set("warps." + warp.getName() + ".z", warp.getZ());
		config.getConfig().set("warps." + warp.getName() + ".yaw", warp.getYaw());
		config.getConfig().set("warps." + warp.getName() + ".pitch", warp.getPitch());
		config.saveConfig(); // save config file
	}

}
