package me.zyrkran.customwarp.messages;

public class Messages {
	
	// ---------------------------------- //
	// 			WARP MESSAGES
	// ---------------------------------- //
	
	public static String NO_PERMISSION = "null";
	public static String MUST_BE_INGAME_PLAYER = "null";
	public static String WARP_EXISTS = "null";
	public static String WARP_NAME_MUST_BE_ALPHANUMERIC = "null";
	public static String WARP_NOT_FOUND = "null";
	public static String WARP_HAS_BEEN_CREATED = "null";
	public static String WARP_MUST_HAVE_NAME = "null";
	public static String WARP_HAS_BEEN_REMOVED = "null";
	public static String WARP_LOCATION_UPDATE_FAILED = "null";
	public static String WARP_LOCATION_UPDATE_SUCCESS = "null";
	public static String WARP_TELEPORTED_TO = "null";
	public static String WARP_SPECIFY_NAME = "null";
	
	// ---------------------------------- //
	// 			SUBWARP MESSAGES
	// ---------------------------------- //
	
	public static String SUBWARP_NOT_FOUND = "null";
	public static String SUBWARP_MUST_HAVE_NAME = "null";
	public static String SUBWARP_HAS_BEEN_CREATED = "null";
	public static String SUBWARP_HAS_BEEN_REMOVED = "null";
	public static String SUBWARP_NAME_MUST_BE_ALPHANUMERIC = "null";
	public static String SUBWARP_TELEPORTED_TO = "null";
	
	// ---------------------------------- //
	// 			EXTRAS MESSAGES
	// ---------------------------------- //

	public static String ERROR_SYNTAX = "null";
	public static String ERROR_TOO_MANY_ARGS = "null";
	public static String ERROR_INVALID_COMMAND = "null";
	public static String PLAYER_SPECIFY_NAME = "null";
	public static String REACHED_MAX_AMOUNT_OF_WARPS = "null";
	public static String MISSING_OWNERSHIP = "null";


}
