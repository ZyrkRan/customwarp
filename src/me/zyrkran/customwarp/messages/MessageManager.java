package me.zyrkran.customwarp.messages;

import me.zyrkran.customwarp.CustomWarp;
import me.zyrkran.customwarp.config.Config;

public class MessageManager {
	
	private Config langConfig;
	
	public MessageManager(CustomWarp plugin){
		langConfig = new Config(plugin, "lang_US.yml");
		langConfig.saveDefaultConfig();
		
		reload();
	}
	
	public void reload(){
		
		// ---------------------------------- //
		// 			WARP MESSAGES
		// ---------------------------------- //
		Messages.NO_PERMISSION 						= getMessage("no-permission");
		Messages.MUST_BE_INGAME_PLAYER 				= getMessage("must-be-ingame-player");
		Messages.WARP_EXISTS 						= getMessage("warp-exists");
		Messages.WARP_NAME_MUST_BE_ALPHANUMERIC 	= getMessage("warp-name-must-be-alphanumeric");
		Messages.WARP_NOT_FOUND 					= getMessage("warp-not-found");
		Messages.WARP_MUST_HAVE_NAME 				= getMessage("warp-must-have-name");
		Messages.WARP_HAS_BEEN_CREATED 				= getMessage("warp-has-been-created");
		Messages.WARP_HAS_BEEN_REMOVED 				= getMessage("warp-has-been-removed");
		Messages.WARP_LOCATION_UPDATE_FAILED 		= getMessage("warp-location-update-failed");
		Messages.WARP_LOCATION_UPDATE_SUCCESS 		= getMessage("warp-location-update-success");
		Messages.WARP_TELEPORTED_TO 				= getMessage("warp-teleported-to");
		Messages.WARP_SPECIFY_NAME	 				= getMessage("warp-specify-name");
		
		// ---------------------------------- //
		// 			SUBWARP MESSAGES
		// ---------------------------------- //
		
		Messages.SUBWARP_NOT_FOUND 					= getMessage("subwarp-not-found");
		Messages.SUBWARP_MUST_HAVE_NAME 			= getMessage("subwarp-must-have-name");
		Messages.SUBWARP_HAS_BEEN_CREATED 			= getMessage("subwarp-has-been-created");
		Messages.SUBWARP_HAS_BEEN_REMOVED			= getMessage("subwarp-has-been-removed");
		Messages.SUBWARP_NAME_MUST_BE_ALPHANUMERIC 	= getMessage("subwarp-name-must-be-alphanumeric");
		Messages.SUBWARP_TELEPORTED_TO 				= getMessage("subwarp-teleported-to");
		
		// ---------------------------------- //
		// 			EXTRAS MESSAGES
		// ---------------------------------- //

		Messages.ERROR_SYNTAX 						= getMessage("error-invalid-syntax");
		Messages.ERROR_TOO_MANY_ARGS 				= getMessage("error-too-many-arguments");
		Messages.ERROR_INVALID_COMMAND 				= getMessage("error-invalid-command");
		Messages.PLAYER_SPECIFY_NAME				= getMessage("player-specify-name");
		Messages.REACHED_MAX_AMOUNT_OF_WARPS 		= getMessage("reached-max-amount-of-warps");
		Messages.MISSING_OWNERSHIP 					= getMessage("missing-ownership");
	}
	
	private String getMessage(String msg_id){
		return langConfig.getConfig().getString(msg_id);
	}

}
