package me.zyrkran.customwarp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.zyrkran.customwarp.commands.CommandCustomWarp;
import me.zyrkran.customwarp.commands.CommandManager;
import me.zyrkran.customwarp.messages.MessageManager;
import me.zyrkran.customwarp.warps.WarpManager;

public class CustomWarp extends JavaPlugin {
	
	private static CustomWarp instance;
	private static WarpManager warpManager;
	private static MessageManager msgManager;
	
	
	/*
	 * NOTES:
	 * 
	 * code returns nullpointer on subwarp overwrite event (CommandManager.java:77)
	 */
	
	
	
	public void onEnable(){
		// declare instance of main class
		instance = this;
		
		// init WarpManager and load available warps/subwarps
		warpManager = new WarpManager(this);
		warpManager.loadWarpDataFromConfig();
		
		// init MessageManager and load messages from config file
		msgManager = new MessageManager(this);
		msgManager.reload();
		
		// register commands
		getCommand("customwarp").setExecutor(new CommandCustomWarp());
		
		// register event
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new CommandManager(warpManager), this);
		
	}
	
	public void onDisable(){
//		warpManager.saveWarpsToDisk();
	}
	
	public static CustomWarp getInstance(){
		return instance;
	}
	
	public static WarpManager getWarpManager(){
		return warpManager;
	}

}
