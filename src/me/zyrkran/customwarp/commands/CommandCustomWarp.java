package me.zyrkran.customwarp.commands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zyrkran.customwarp.CustomWarp;
import me.zyrkran.customwarp.exceptions.InvalidStringException;
import me.zyrkran.customwarp.exceptions.WarpAlreadyExistsException;
import me.zyrkran.customwarp.exceptions.WarpNotFoundException;
import me.zyrkran.customwarp.messages.Messages;
import me.zyrkran.customwarp.utils.Chat;
import me.zyrkran.customwarp.warps.Warp;
import me.zyrkran.customwarp.warps.WarpManager;
import net.md_5.bungee.api.ChatColor;

public class CommandCustomWarp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(Messages.MUST_BE_INGAME_PLAYER);
			return false;
		}
		
		Player player  = (Player) sender;
		
		// ----------------------------- //
		//     HELP|? SUBCOMMAND
		// ----------------------------- //
		if (args.length == 0 || (args.length == 1 && (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")))){
			if (!player.hasPermission("customwarps.admin")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}
			displayHelp(player);
			return true;
		}

		// ----------------------------- //
		//     LIST SUBCOMMAND
		// ----------------------------- //
		else if (args[0].equalsIgnoreCase("list")){
			if (!player.hasPermission("customwarp.list")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}
			
			List<Warp> warpList = CustomWarp.getWarpManager().getWarps();
			
			// send player a list of warps
			StringBuilder sb = new StringBuilder();
			sb.append(ChatColor.GOLD + "Available warps (" + warpList.size() + "): " + ChatColor.WHITE);
			
			if (warpList.isEmpty()){
				sb.append("none");
				Chat.sendMessage(player, sb.toString());
				return true;
			}
			
			// append warp names 
			for (Warp warp : warpList){
				sb.append(warp.getName() + ", ");
			}
			
			// Remove the last comma
			sb.deleteCharAt(sb.length()-2);
			
			// Send list of warps to the player
			Chat.sendMessage(player, sb.toString());
			return true;
		}

		// ----------------------------- //
		//     UPDATE SUBCOMMAND
		// ----------------------------- //
		else if (args[0].equalsIgnoreCase("update")){
			if (!player.hasPermission("customwarp.update")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}
			
			if (args.length == 1){
				Chat.sendMessage(player, Messages.WARP_SPECIFY_NAME);
				return false;
			}
			
			else if (args.length > 2){
				Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
				return false;
			}

			Warp warp = WarpManager.getWarpByName(args[1]);
			
			if (warp == null){
				Chat.sendMessage(player, Messages.WARP_NOT_FOUND);
				return false;
			}
			
			try {
				CustomWarp.getWarpManager().updateWarpLocation(args[1], player.getLocation());
			} catch (WarpNotFoundException e) {
				Chat.sendMessage(player, Messages.WARP_LOCATION_UPDATE_FAILED);
			}
			Chat.sendMessage(player, Messages.WARP_LOCATION_UPDATE_SUCCESS);
		}
		
		// ----------------------------- //
		//     WARP SUBCOMMAND
		// ----------------------------- //
		else if (args[0].equalsIgnoreCase("warp")){
			if (!player.hasPermission("customwarp.warp")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}

			if (args.length == 1){
				Chat.sendMessage(player, Messages.WARP_SPECIFY_NAME);
				return false;
			}
			
			else if (args.length > 2){
				Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
				return false;
			}
			
			Warp warp = WarpManager.getWarpByName(args[1]);
			
			if (warp == null){
				Chat.sendMessage(player, Messages.WARP_NOT_FOUND);
				return false;
			}
			
			player.teleport(warp.getLocation());
			Chat.sendMessage(player, Messages.WARP_TELEPORTED_TO);
			return true;
		}
		
		// ----------------------------- //
		//     CREATE SUBCOMMAND
		// ----------------------------- //
		else if (args[0].equalsIgnoreCase("create")){
			if (!player.hasPermission("customwarp.createwarps")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}
			
			if (args.length == 1){
				Chat.sendMessage(player, Messages.WARP_MUST_HAVE_NAME);
				return false;
			}
			
			else if (args.length > 2){
				Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
				return false;
			}

			
			// attempt to create a new warp
			try {
				CustomWarp.getWarpManager().tryCreateWarp(player, args[1], player.getLocation());
				Chat.sendMessage(player, Messages.WARP_HAS_BEEN_CREATED);
			} catch (WarpAlreadyExistsException e) {
				Chat.sendMessage(player, Messages.WARP_EXISTS);
			} catch (InvalidStringException e){
				Chat.sendMessage(player, Messages.WARP_NAME_MUST_BE_ALPHANUMERIC);
			}
		}
			
			
		// ----------------------------- //
		//     REMOVE SUBCOMMAND
		// ----------------------------- //

		else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")){
			if (!player.hasPermission("customwarp.removewarps")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}
			
			// Did not specify a warp name
			if (args.length == 1){
				Chat.sendMessage(player, Messages.WARP_SPECIFY_NAME);
				return false;
			}

			// attempt to remove warp
			try {
				CustomWarp.getWarpManager().tryRemoveWarp(player, args[1]);
				Chat.sendMessage(player, Messages.WARP_HAS_BEEN_REMOVED);
			} catch (WarpNotFoundException e) {
				Chat.sendMessage(player, Messages.WARP_NOT_FOUND);
			} catch (InvalidStringException e) {
				Chat.sendMessage(player, Messages.SUBWARP_NAME_MUST_BE_ALPHANUMERIC);
			}
		}

		// ----------------------------- //
		//     INFO SUBCOMMAND
		// ----------------------------- //

		else if (args[0].equalsIgnoreCase("info")){
			if (!player.hasPermission("customwarp.warpinfo")){
				Chat.sendMessage(player, Messages.NO_PERMISSION);
				return false;
			}
			
			if (args.length == 1){
				Chat.sendMessage(player, Messages.WARP_SPECIFY_NAME);
				return false;
			}
			
			if (args.length > 2){
				Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
				return false;
			}
			
			Warp warpObj = WarpManager.getWarpByName(args[1]);
			
			if (warpObj == null){
				Chat.sendMessage(player, Messages.SUBWARP_NOT_FOUND);
				return false;
			}
			
			Chat.sendMessage(player, "&6Information about '&a" + warpObj.getName() + "&6'");
			Chat.sendMessage(player, "&7Created by: &f" + Bukkit.getOfflinePlayer(warpObj.getCreatorUUID()).getName());
			Chat.sendMessage(player, "&7Type: &fWARP" );
			Chat.sendMessage(player, "&7Location: &f" + 
					warpObj.getWorldName() + ", " +
					(int)warpObj.getX() + ", " +
					(int)warpObj.getY() + ", " +
					(int)warpObj.getZ() + ", ");
			return true;
		}
		
		else {
			Chat.sendMessage(player, Messages.ERROR_INVALID_COMMAND);
			return false;
		}
		return false;
	}
	
	private void displayHelp(Player player){
		Chat.sendMessage(player, "&b-----------------------------------");
		Chat.sendMessage(player, "&b<> - Required | [] - Optional");
		Chat.sendMessage(player, "&b/customwarp warp <name>");
		Chat.sendMessage(player, "&b/customwarp create <name>");
		Chat.sendMessage(player, "&b/customwarp remove|delete <name>");
		Chat.sendMessage(player, "&b/customwarp update <name>");
		Chat.sendMessage(player, "&b/customwarp info <name>");
		Chat.sendMessage(player, "&b/customwarp list");
		Chat.sendMessage(player, "&b-----------------------------------");
	}

}
