package me.zyrkran.customwarp.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import me.zyrkran.customwarp.CustomWarp;
import me.zyrkran.customwarp.exceptions.InvalidStringException;
import me.zyrkran.customwarp.exceptions.MissingOwnershipException;
import me.zyrkran.customwarp.exceptions.SubWarpNotFoundException;
import me.zyrkran.customwarp.messages.Messages;
import me.zyrkran.customwarp.utils.Chat;
import me.zyrkran.customwarp.warps.Subwarp;
import me.zyrkran.customwarp.warps.Warp;
import me.zyrkran.customwarp.warps.WarpManager;
import net.md_5.bungee.api.ChatColor;

public class CommandManager implements Listener{
	
	private WarpManager manager;
	private static List<String> commands;
	
	public CommandManager(WarpManager manager){
		this.manager = manager;
		commands = new ArrayList<String>();
		loadCommands();
	}
	
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onCommand(PlayerCommandPreprocessEvent event){
		Player player = event.getPlayer();
		String cmd = event.getMessage();
		String args[] = cmd.split(" ");
		
		for (String s : commands){
			if (args[0].equalsIgnoreCase("/" + s)){
				event.setCancelled(true);// stop the "unknown command" message from appearing				
				
				// ------------------------------------------ //
				// FOUND CUSTOM COMMAND, PROCESS IT
				// ------------------------------------------ //
				
				if (args.length == 1 || args[1].equalsIgnoreCase("help") || args[1].equalsIgnoreCase("?") ){
					String a = StringUtils.remove(args[0], "/");
					
					Chat.sendMessage(player, ChatColor.AQUA + "-----------------------------------");
					Chat.sendMessage(player, ChatColor.AQUA + "<> - Required | [] - Optional");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " warp [name]");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " create|set <name>");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " remove|delete <name>");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " update <name>");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " assets|limits [player]");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " list|warps");
					Chat.sendMessage(player, ChatColor.AQUA + "/" + a + " info|stats <name> ");
					Chat.sendMessage(player, ChatColor.AQUA + "-----------------------------------");
					return;
				}
				
				// useful variables
				String warp_name = WarpManager.getWarpCaseSentitiveName(StringUtils.remove(args[0], "/"));
				Warp warpObj = WarpManager.getWarpByName(warp_name);
				
				// ----------------------------- //
				//     CREATE SUBCOMMAND
				// ----------------------------- //
				// <warpName> create|set <name>
				if (args[1].equalsIgnoreCase("create") || args[1].equalsIgnoreCase("set")){
					if (args.length == 3){
						
						boolean canSetWarp = WarpManager.playerHasPermissionToSetWarp(player);
						
						if (canSetWarp || player.isOp() || player.hasPermission("customwarp.subwarps.bypass")){
							// try to set warp
							try {
								manager.tryCreateSubWarp(player, args[2], warp_name);
								Chat.sendMessage(player, Messages.SUBWARP_HAS_BEEN_CREATED);
								return;
							} catch (InvalidStringException e) {
								Chat.sendMessage(player, Messages.SUBWARP_NAME_MUST_BE_ALPHANUMERIC);
							} catch (MissingOwnershipException e) {
								Chat.sendMessage(player, Messages.MISSING_OWNERSHIP);
							}
						} else {
							Chat.sendMessage(player, Messages.REACHED_MAX_AMOUNT_OF_WARPS);
						}
					} 
					
					else if (args.length > 3){
						Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
						return;
					}

					else {
						Chat.sendMessage(player, Messages.SUBWARP_MUST_HAVE_NAME);
						return;
					}
				}

				
				// ----------------------------- //
				//     REMOVE SUBCOMMAND
				// ----------------------------- //
				// <warpName> remove <name>
				if (args[1].equalsIgnoreCase("remove") || args[1].equalsIgnoreCase("delete")){
					if (args.length == 3){
						try {
							manager.tryRemoveSubWarp(player, args[2], warp_name);
							Chat.sendMessage(player, Messages.SUBWARP_HAS_BEEN_REMOVED);
							return;
						} catch (SubWarpNotFoundException e) {
							Chat.sendMessage(player, Messages.SUBWARP_NOT_FOUND);
						}
					} 
					
					else if (args.length > 3){
						Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
						return;
					}
					
					else {
						Chat.sendMessage(player, Messages.SUBWARP_MUST_HAVE_NAME);
						return;
					}
				}

				
				// ----------------------------- //
				//     UPDATE SUBCOMMAND
				// ----------------------------- //
				// <warpname> update <warp>
				if (args[1].equalsIgnoreCase("update")){
					if (args.length == 2){
						Chat.sendMessage(player, Messages.WARP_SPECIFY_NAME);
						return;
					}
					
					if (args.length > 3){
						Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
						return;
					}
					
					try{
						manager.tryCreateSubWarp(player, args[2], warp_name);
						Chat.sendMessage(player, Messages.WARP_LOCATION_UPDATE_SUCCESS);
					} catch (InvalidStringException e){
						Chat.sendMessage(player, Messages.WARP_LOCATION_UPDATE_FAILED);
					} catch (MissingOwnershipException e){
						Chat.sendMessage(player, Messages.MISSING_OWNERSHIP);
					}
				}

				
				// ----------------------------- //
				//     WARP SUBCOMMAND
				// ----------------------------- //
				// <warpName> warp [name]
				if (args[1].equalsIgnoreCase("warp")){
					Warp warp = WarpManager.getWarpByName(StringUtils.remove(s, '/'));
					
					// player wants to teleport to warp
					if (args.length == 2){
						player.teleport(warp.getLocation());
						Chat.sendMessage(player, Messages.WARP_TELEPORTED_TO);
						return;
					}
					
					// player wants to teleport to subwarp
					else if (args.length == 3){
						Subwarp subwarp = warp.getSubwarpByName(StringUtils.remove(args[2], '/'));
						
						System.out.println(subwarp);
						if (subwarp == null){
							Chat.sendMessage(player, Messages.SUBWARP_NOT_FOUND);
							return;
						}
						
						player.teleport(subwarp.getLocation());
						Chat.sendMessage(player, Messages.SUBWARP_TELEPORTED_TO);
						return;
					}
				}
				
				
				// ----------------------------- //
				//     LIST SUBCOMMAND
				// ----------------------------- //
				// <warpName> list
				if (args[1].equalsIgnoreCase("list") || args[1].equalsIgnoreCase("warps")){
					List<Subwarp> subwarpList = warpObj.getSubwarps();
					
					// send player a list of warps
					StringBuilder sb = new StringBuilder();
					sb.append(ChatColor.GOLD + "Available subwarps (" + subwarpList.size() + "): " + ChatColor.WHITE);
					
					if (subwarpList.isEmpty()){
						sb.append("none");
						Chat.sendMessage(player, sb.toString());
						return;
					}
					
					// append warp names 
					for (Subwarp warp : subwarpList){
						sb.append(warp.getName() + ", ");
					}
					
					// Remove the last comma
					sb.deleteCharAt(sb.length()-2);
					
					// Send list of warps to the player
					Chat.sendMessage(player, sb.toString());
					return;
				}
				
				
				// ----------------------------- //
				//     ASSETS SUBCOMMAND
				// ----------------------------- //
				// <warpName> assets|limits <player>
				if (args[1].equalsIgnoreCase("assets") || args[1].equalsIgnoreCase("limits")){
					
					// player wants to teleport to warp
					if (args.length == 2){
						Chat.sendMessage(player, Messages.PLAYER_SPECIFY_NAME);
						return;
					}
					
					// player wants to teleport to subwarp
					else if (args.length == 3){
						List<Subwarp> warpsByPlayer = WarpManager.getTotalWarpsByPlayer(player);
						
						// send player a list of warps
						StringBuilder sb = new StringBuilder();
						sb.append(ChatColor.GOLD + "Created warps (" + warpsByPlayer.size() + "): " + ChatColor.WHITE);
						
						if (warpsByPlayer.isEmpty()){
							sb.append("none");
							Chat.sendMessage(player, sb.toString());
							return;
						}
						
						// append warp names 
						for (Subwarp warp : warpsByPlayer){
							sb.append(warp.getName() + ", ");
						}
						
						// Remove the last comma
						sb.deleteCharAt(sb.length()-2);
						
						// Send list of warps to the player
						Chat.sendMessage(player, sb.toString());
						return;
					}
				}
				
				
				// ----------------------------- //
				//     INFO SUBCOMMAND
				// ----------------------------- //
				// <warpname> info <warp>
				if (args[1].equalsIgnoreCase("info") || args[1].equalsIgnoreCase("stats")){
					if (args.length == 2){
						Chat.sendMessage(player, Messages.WARP_SPECIFY_NAME);
						return;
					}
					
					if (args.length > 3){
						Chat.sendMessage(player, Messages.ERROR_TOO_MANY_ARGS);
						return;
					}
					
					Subwarp subwarpObj = WarpManager.getWarpByName(warp_name).getSubwarpByName(args[2]);
					
					if (subwarpObj == null){
						Chat.sendMessage(player, Messages.SUBWARP_NOT_FOUND);
						return;
					}
					
					Chat.sendMessage(player, "&7Information about '&a" + subwarpObj.getName() + "&6'");
					Chat.sendMessage(player, "&7Created by: &f" + Bukkit.getOfflinePlayer(subwarpObj.getCreatorUUID()).getName());
					Chat.sendMessage(player, "&7Location: &f" + 
							subwarpObj.getWorldName() + ", " +
							(int)subwarpObj.getX() + ", " +
							(int)subwarpObj.getY() + ", " +
							(int)subwarpObj.getZ() + ", ");
					return;
				}
			}
		}
	}
	
	public void loadCommands(){
		for (Warp warp : CustomWarp.getWarpManager().getWarps()){
			commands.add(warp.getName().toLowerCase());
		}
	}
	
	public static void registerCommand(String string){
		commands.add(string);
	}
	
	public static void unregisterCommand(String string){
		commands.remove(string);
	}

}
